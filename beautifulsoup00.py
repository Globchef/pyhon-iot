# sudo apt install python-bs4
'''
simple demo of beautiful soup on the home page of imal
'''
from bs4 import BeautifulSoup
import requests

URL = "https://imal.org"

r = requests.get(URL)
soup = BeautifulSoup(r.content)

#print( soup.prettify() )

print( soup.title.string )

for link in soup.find_all('a'):
	print(link.get('href'))

for div in soup.find_all('div',{'class':'content'}):
	divc = link.get('class')
	print( '####################################' )
	#print(div.encode_contents())
	print(div.contents)