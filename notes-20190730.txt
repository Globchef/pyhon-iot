Objets connectés / Raspbery Pi + Python @ iMAL (30 juillet - 2 août)

trucs utiles:

Avant de venir
		2. Pendant cette semaine, nous allons nous connecter au Raspberry Pi grâce à  SSH 
			3. Sous macOS & Linux, vous pourrez le faire directement à partir du terminal. 
			3. Sous Windows, il vous faudra installer un client SSH, soit  PuTTY (télécharger et installer le "Windows Installer"), ou  KiTTY (télécharger au minimum kitty.exe)
		2. Pour retrouver votre Raspberry Pi facilement sur le réseau, 
			3. les utilisateurs Windows devront également installer  Bonjour (à partir de la version 1809 de Windows 10, cela devrait normalement ne plus être nécessaire ;  comment connaître sa version de Windows), 
			3. les utilisateurs de Linux pourraient devoir installer l'équivalent open source, Avahi. 
			3. Sous macOS, tout est déjà prêt.

router:
- 10.3.14.* / 255.255.255.0
- SSID : piwifi
- PWD : piwifimal

sources du cours: https://gitlab.com/frankiezafe/pyhon-iot
connection à la raspberry
- ssh pi@10.3.14.** : lancer des commandes
- sftp pi@10.3.14.** : upload et download de fichiers depuis votre machine

une fois en ssh, commandes utiles:
- ls : liste des fichiers dans le répertoire courant
- cd [nom du dossier] : changment de dossier
-- cd / : racine di système
-- cd ~ : racine de l'utilisateur
-- cd .. : remonter d'un niveau
- mv [nom du fichier/dossier] [nouveau nom du fichier/dossier] : renommer un fichier ou un dossier
- cp [chemin du fichier] [nouveau chemin du fichier] : copie un fichier
- cp -r [chemin du dossier] [nouveau chemin du dossier] : copie un dossier et tout son contenu
- mkdir [nom de dossier] : créé un dossier

une fois en sftp, commandes utiles:
- ls, cd, mv, cp & mkdir sont toujours disponibles
- put [chemin du fichier local] [dossier sur la machine distante] : upload un fichier vers la machine distante
- get [chemin du fichier sur la machine distante] [dossier local] : download un fichier