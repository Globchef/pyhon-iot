import os
try: 
	from http.server import HTTPServer, SimpleHTTPRequestHandler # Python 3
except ImportError: 
	from SimpleHTTPServer import BaseHTTPServer
	HTTPServer = BaseHTTPServer.HTTPServer
	from SimpleHTTPServer import SimpleHTTPRequestHandler # Python 2

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1113
IMAGE_FOLDER = 'html/images'

# getting the list of images for folder html/images
all_img = []
all_img_path = []
for item in os.listdir(IMAGE_FOLDER):
	all_img.append( item )
	all_img_path.append( os.path.join( IMAGE_FOLDER, item ) )
	
class Handler(SimpleHTTPRequestHandler):
	
	out = ""
	
	def boom( self ):
		self.out = ""
		self.out += "invalid image number, choose below:<br/>"
		for i in range(0,len(all_img)):
			self.out += str(i) + " : " + all_img[i] + "<br/>"
	
	def load( self, i ):
		
		if i < 0 or i > len( all_img ):
			raise Exception('Wrong image number request!')
		
		self.out = ""
		self.out += "<h1>" + all_img[i] + "</h1>"
		self.out += '<img src="' + all_img_path[i] + '"/><br/>'
		if i > 0:
			self.out += '<a href="' + str( i - 1 ) + '">previous</a><br/>'
		if i < len( all_img ) - 1:
			self.out += '<a href="' + str( i + 1 ) + '">next</a><br/>'
	
	def do_GET(self):
		
		try:
			img_num = int(str(self.path[1:]))
			self.load( img_num )
		except:
			self.boom()
		
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(self.out.encode("utf-8"))
		
		return
		

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), Handler)

print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )

SERVER.serve_forever()