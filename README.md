# Objets connectés / Raspbery Pi + Python

imal, 30 juillet - 2 août, https://legacy.imal.org/summerworkshops2019/raspberrypi#python

scripts and notes of the workshop

## base du web

**web00_server.py** : basic file server (start it in the *html* folder with `python ../web00_server.py` to make visible the base root stuff)

### GET requests

**web01_server.py** : parsing GET requests, and responding to the browser

**web02_server.py** : changing returned message depending on the get parameters

**web03_server.py** : adapting header to send images, big piece of code, go smoothly!

### POST requests

**web04_server.py** : parsing POST requests and saving files on drive

**web05_server.py** : listing files uploaded & providing links to files

### scrapping

**beautifulsoup00.py** : simple example on how to extract content from a webpage

requires the installation of python-bs4, on linux:

```bash
sudo apt install python-bs4
```

**beautifulsoup01.py** : advanced example using selenium's webdriver

requirements:

works on linux desktop 64 bits, for raspberry, see below

```bash
sudo apt install python-bs4
sudo apt-get install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
sudo apt-get install -f
sudo apt-get install xvfb
sudo apt-get install unzip
wget -N http://chromedriver.storage.googleapis.com/2.26/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
chmod +x chromedriver
sudo mv -f chromedriver /usr/local/share/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
sudo ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
sudo -H python3 -m pip install pyvirtualdisplay selenium
```

on raspberry:

- https://raspberrypi.stackexchange.com/questions/4941/can-i-run-selenium-webdriver-using-firefox-as-the-browser

source: https://christopher.su/2015/selenium-chromedriver-ubuntu/

test your installation with the script **selenium_chrome.py**

**tweetscrapper.py** : get all tweets from a user, depending on twitter definition of 'all'

### subprocess

to launch programs with python, use subprocess.call, see [documentation](https://docs.python.org/2/library/subprocess.html) for more details

**vlc_launcher.py** : play a video in fullscreen with vlc

### during course

**webserver.py** : script developped during the first day

## resources

### links

- [Simple HTTP server](https://docs.python.org/2/library/simplehttpserver.html)
- [Basic example of GET and POST management](https://www.codexpedia.com/python/python-web-server-for-get-and-post-requests/)
- [Serving an image with Simple HTTP Server](https://www.reddit.com/r/learnpython/comments/1sendp/display_image_on_browser_using_python_http_server/)
- [Checking fields in a POST form](https://blog.anvileight.com/posts/simple-python-http-server/#do-post)
- [Saving file in a POST form](https://stackoverflow.com/questions/49734694/how-to-upload-files-to-server-from-html-form-using-python-cgi)
- [Documentation about CGI](https://docs.python.org/2/library/cgi.html)

- [Build a Python Web Server with Flask](https://projects.raspberrypi.org/en/projects/python-web-server-with-flask)
- [Flask documentation](https://flask.palletsprojects.com/en/1.1.x/)

- [All possible Content-Type for HTTP headers](https://stackoverflow.com/questions/23714383/what-are-all-the-possible-values-for-http-content-type-header#37416922)

- [Beautiful soup official website](https://www.crummy.com/software/BeautifulSoup/)
- [Beautiful soup documentation](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors)
- [Beautiful soup for beginners](https://www.pythonforbeginners.com/beautifulsoup/beautifulsoup-4-python)
- [Installation of selenium with chrome driver](https://christopher.su/2015/selenium-chromedriver-ubuntu/)

- [Detect keyboard events](https://pythonhosted.org/pynput/keyboard.html)

all images of folder *html/images* are from https://libreshot.com/tag/red/ and uses a CC0 license

### about beautiful soup

to find nested tags, use this kind of call: loop over the first search, it will returns tags, and apply a new filter on these tags

the example below is retrieving H2 text of a wikipedia page

```python
for h2 in soup.find_all('h2'):
	for ptag in h2.find_all('span',{'class','mw-headline'}):
		print(ptag.text)
```

source: https://stackoverflow.com/questions/46510966/beautiful-soup-nested-tag-search?rq=1

with css selectors (same effect as above):

```python
for c in soup.select('h2 .mw-headline'):
    print(c.string)
```

source: https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors

### usefull commands

```bash
killall python3
python3 webserver.py
sudo python3 -m pip install
sudo -H pip3 install --upgrade pip
sudo -H python3 -m pip install selenium
```