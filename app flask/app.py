from flask import Flask, render_template, jsonify
from servo import *
import RPi.GPIO as GPIO
import random
from time import sleep

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

pwm = GPIO.PWM(12, 50)
pwm.start(0)

app = Flask(__name__)

last_valid_value = value

@app.route('/')
def index():
    return render_template('index.html', value=value)

@app.route('/up/')
def up():
    global last_valid_value
    last_valid_value += 5
    last_valid_value %= 180
    setAngle(last_valid_value)
    return jsonify(last_valid_value)

@app.route('/down/')
def down():
    global last_valid_value
    last_valid_value -= 5
    while last_valid_value < 0:
        last_valid_value += 180
    setAngle(last_valid_value)
    return jsonify(last_valid_value)

@app.route('/get_value/')
def get_value():
    return jsonify(last_valid_value)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
