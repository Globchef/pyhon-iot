import os
try: 
	from http.server import HTTPServer, SimpleHTTPRequestHandler # Python 3
except ImportError: 
	from SimpleHTTPServer import BaseHTTPServer
	HTTPServer = BaseHTTPServer.HTTPServer
	from SimpleHTTPServer import SimpleHTTPRequestHandler # Python 2

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1114
IMAGE_FOLDER = 'html/images'

# getting the list of images for folder html/images
all_img = []
all_img_path = []
for item in os.listdir(IMAGE_FOLDER):
	all_img.append( item )
	all_img_path.append( os.path.join( IMAGE_FOLDER, item ) )
	
class Handler(SimpleHTTPRequestHandler):
	
	out = ""
	
	def boom( self ):
		self.out = ""
		self.out += "invalid image number, choose below:<br/>"
		for i in range(0,len(all_img)):
			self.out += '<a href="/' + str(i) + '">' + str(i) + "</a> : " + all_img[i] + "<br/>"
	
	def load( self, i ):
		
		if i < 0 or i > len( all_img ):
			raise Exception('Wrong image number request!')
		
		self.out = ""
		self.out += "<h1>" + all_img[i] + "</h1>"
		next_i = (i + 1) % len( all_img )
		self.out += '<a href="' + str(next_i) + '"><img width="300" src="' + all_img_path[i] + '"/></a><br/>'
		if i > 0:
			self.out += '<a href="' + str( i - 1 ) + '">previous</a>'
		else:
			self.out += 'previous'
		self.out += ' || '
		if i < len( all_img ) - 1:
			self.out += '<a href="' + str( i + 1 ) + '">next</a>'
		else:
			self.out += 'next'
			
	
	def do_GET(self):
		
		jpg = self.path.rfind('.jpg')
		if jpg > 0:
			try:
				im = str(self.path[1:])
				im_size = os.stat(im).st_size
				im_data = open(im, 'rb')			
				self.send_response(200)
				self.send_header("Content-type", "image/jpg")
				self.send_header("Content-length", im_size)
				self.end_headers()
				self.wfile.write(im_data.read())
				im_data.close()
			except IOError:
				self.send_error(404, "File not found")
			return
		
		try:
			img_num = int(str(self.path[1:]))
			self.load( img_num )
		except:
			self.boom()
		
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(self.out.encode("utf-8"))
		
		return
		

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), Handler)

print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )

SERVER.serve_forever()