import os
import io
import cgi
try: 
	# Python 3
	from http.server import HTTPServer, SimpleHTTPRequestHandler
	from urllib.parse import parse_qs
except ImportError: 
	# Python 2
	from SimpleHTTPServer import BaseHTTPServer
	HTTPServer = BaseHTTPServer.HTTPServer
	from SimpleHTTPServer import SimpleHTTPRequestHandler
	from urlparse import parse_qs

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1115
UPLOAD_FOLDER = 'html/upload'

class Handler(SimpleHTTPRequestHandler):
	
	def do_GET(self):
		
		# multipart/form-data
		# application/x-www-form-urlencoded
		
		txt = '''file upload<br/>
		<form enctype="multipart/form-data" method="POST">
		<input id="fileupload" name="file" type="file" />
		<input type="submit" value="submit" id="submit" />
		</form>'''
		
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(txt.encode("utf-8"))
		
		return
	
	def do_POST(self):
		
		form = cgi.FieldStorage(self.rfile, headers=self.headers, environ={'REQUEST_METHOD':'POST','CONTENT_TYPE':self.headers['Content-Type'],})
		
		if "file" in form:
			
			form_file = form['file']
			
			if form_file.filename:
			
				uploaded_file_path = os.path.join(UPLOAD_FOLDER, os.path.basename(form_file.filename))
				
				with open(uploaded_file_path, 'wb') as fout:
					# read the file in chunks as long as there is data
					while True:
						chunk = form_file.file.read(100000)
						if not chunk:
							break
						# write the file content on a file on the hdd
						fout.write(chunk)
				
				txt = 'file %s uploaded' % (uploaded_file_path)
				self.send_response(200)
				self.send_header('Content-Type', 'text/html')
				self.end_headers()
				self.wfile.write(txt.encode("utf-8"))
				return
		
		txt = 'something went awfully wrong...'
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(txt.encode("utf-8"))

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), Handler)

print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )

SERVER.serve_forever()