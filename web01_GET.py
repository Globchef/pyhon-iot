try: 
	from http.server import HTTPServer, SimpleHTTPRequestHandler # Python 3
except ImportError: 
	from SimpleHTTPServer import BaseHTTPServer
	HTTPServer = BaseHTTPServer.HTTPServer
	from SimpleHTTPServer import SimpleHTTPRequestHandler # Python 2

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1112

class Handler(SimpleHTTPRequestHandler):
	
	def do_GET(self):
		
		print(self.path)
		
		out = "You have sent me '" + str( self.path ) + "', am i right?"
		
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(out.encode("utf-8"))
		
		return
		

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), Handler)

print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )

SERVER.serve_forever()