# sudo apt install python-bs4
# pip3 install beautifulsoup4
'''
simple demo of beautiful soup on the home page of imal
'''
from bs4 import BeautifulSoup
import requests

#URL = "https://imal.org"
#URL = "https://twitter.com/hashtag/python?src=hash"
URL = "https://en.wikipedia.org/wiki/Scrap"

r = requests.get(URL)

#print( r.content )

soup = BeautifulSoup(r.content)

#print( soup.prettify() )
#print( soup.title.string )

for h2 in soup.find_all('h2'):
	for ptag in h2.find_all('span',{'class','mw-headline'}):
		print(ptag.text)

'''
for link in soup.find_all('a'):
	print(link.get('href'))

for link in soup.find_all('img'):
	print(link.get('src'))

for div in soup.find_all('div',{'class':'content'}):
	divc = link.get('class')
	print( '####################################' )
	#print(div.encode_contents())
	print(div.contents)
'''